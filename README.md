![Alt text](http://www.gusl.co/blast/resources/images/b-circle-trans-100.png) *on* ![Alt text](http://vertx.io/assets/logo-sm.png)

Built using Vertx version 3.4.1

# Gradle
```
repositories { maven { url "http://dl.bintray.com/gusl/blast" } }

compile 'co.gusl:blast-engine-vertx:1.0.0'
```

# Embedded

## vertx-web

If using vertx-web, then simply add a new route to the router as follows.

```java
    Router router = Router.router(vertx);
    BlastServer blastServer = Blast.blast( modules ...);
    BlastRequestHandler handler = new BlastRequestHandler(blastServer);
    //Bind Blast
    router.route(Blast.BLAST_ROUTE_PATH).handler(routingContext -> {
        handler.handle(routingContext.request());
    });
```

## vertx-core

```java
        httpServer.requestHandler(new BlastRequestHandler(server));
```

# Stand alone

To run the Standalone version of blast use ..

```java
     BlastServer blast = Blast.blast(new VertxEngine())
```
This will create a vertx instance, and http server, and configure using the properties from Blast Properties.
As the `VertxEngine` is a `Controllable` Blast will start and stop the engine.

## Gradle Runner
`./gradlew vertx:vertx-engine:run`


