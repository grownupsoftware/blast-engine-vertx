/*
 * Grownup Software Limited.
 */
package blast.vertx.engine;

import static blast.BlastConstants.BLAST_ROUTE_PATH;
import static blast.BlastConstants.UPGRADE_HEADER_KEY;
import static blast.BlastConstants.UPGRADE_WEBSOCKET_VALUE;
import blast.client.WebSocketRequestDetailsImpl;
import blast.log.BlastLogger;
import blast.server.BlastServer;
import blast.vertx.client.VertxServerClient;
import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.ServerWebSocket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Vertx Websocket Handler.
 *
 * @author dhudson - Mar 21, 2017 - 12:08:41 PM
 */
public class BlastRequestHandler implements Handler<HttpServerRequest> {

    private static final BlastLogger logger = BlastLogger.createLogger();

    private final BlastServer blastServer;

    public BlastRequestHandler(BlastServer server) {
        blastServer = server;
    }

    @Override
    public void handle(HttpServerRequest request) {
        logger.debug("Vertx request of .. {} {}", request, request.path());

        if (request.path().startsWith(BLAST_ROUTE_PATH)) {
            String value = request.getHeader(UPGRADE_HEADER_KEY);
            if (UPGRADE_WEBSOCKET_VALUE.equals(value)) {
                // Lets upgrade it
                ServerWebSocket websocket = request.upgrade();
                VertxServerClient client = new VertxServerClient(blastServer,
                        websocket, createWebSocketDetails(request));
                client.postClientConnectingEvent();
            }
        }
    }

    public static WebSocketRequestDetailsImpl createWebSocketDetails(HttpServerRequest request) {
        WebSocketRequestDetailsImpl details = new WebSocketRequestDetailsImpl();
        details.setOrigin(request.getHeader("origin"));
        details.setQueryString(request.query());
        details.setRequestPath(request.path());
        details.setHeaders(convertFromMultiMap(request.headers()));
        details.setParams(convertFromMultiMap(request.params()));

        try {
            details.setRequestURI(new URI(request.absoluteURI()));
        } catch (URISyntaxException ignore) {
        }

        details.setRemoteAddress(request.remoteAddress().host());
        details.resolveRemoteAddress();
        return details;
    }

    private static Map<String, List<String>> convertFromMultiMap(MultiMap map) {
        Map<String, List<String>> newMap = new HashMap<>();
        map.names().forEach((header) -> {
            newMap.put(header, map.getAll(header));
        });
        return newMap;
    }
}
