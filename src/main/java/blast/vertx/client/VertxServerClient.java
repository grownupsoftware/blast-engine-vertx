/*
 * Grownup Software Limited.
 */
package blast.vertx.client;

import blast.client.AbstractBlastServerClient;
import blast.client.ClosingReason;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.http.WebSocketFrame;
import java.io.IOException;
import blast.client.WebSocketRequestDetails;
import blast.server.BlastServer;

/**
 * Vertx Blast Server Client.
 *
 * This is the server side client.
 *
 * @author dhudson - Mar 21, 2017 - 9:01:15 AM
 */
public class VertxServerClient extends AbstractBlastServerClient {

    private final ServerWebSocket websocket;
    private final String clientID;

    public VertxServerClient(BlastServer server, ServerWebSocket websocket,
            WebSocketRequestDetails details) {
        super(server, details);
        this.websocket = websocket;
        clientID = createShortClientID();

        websocket.frameHandler((WebSocketFrame frame) -> {
            postClientInboundMessageEvent(frame.textData());
        });

        websocket.closeHandler(ch -> {
            close(ClosingReason.CLOSED_BY_CLIENT);
        });
    }

    @Override
    public String getClientID() {
        return clientID;
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        try {
            websocket.writeFinalTextFrame(new String(bytes));
        } catch (IllegalStateException ignore) {
            // Client is closed.
        }
    }

    @Override
    public void close(ClosingReason reason) {
        if (reason == ClosingReason.CLOSED_BY_SERVER) {
            websocket.close();
        }

        postClientClosed(reason);
    }

    @Override
    public void queueMessage(byte[] bytes) {
        postQueuedMessageEvent(bytes);
    }
}
