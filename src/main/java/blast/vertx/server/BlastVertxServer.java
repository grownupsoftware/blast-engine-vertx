/*
 * Grownup Software Limited.
 */
package blast.vertx.server;

import blast.Blast;
import static blast.BlastConstants.BLAST_ROUTE_PATH;
import blast.exception.BlastException;
import blast.module.echo.EchoModule;
import blast.module.ping.PingModule;
import blast.server.BlastServer;
import blast.server.config.BlastProperties;
import blast.vertx.engine.BlastRequestHandler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;

/**
 * Bootstrap class.
 *
 * Standalone Vertx - Blast.
 *
 * @author dhudson - Mar 20, 2017 - 1:55:25 PM
 */
public class BlastVertxServer {

    public static void main(String[] args) {
        try {

            Vertx vertx = Vertx.vertx();
            Router router = Router.router(vertx);
            BlastServer blastServer = Blast.blast(new PingModule(), new EchoModule());
            BlastRequestHandler handler = new BlastRequestHandler(blastServer);

            //Bind Blast
            router.route(BLAST_ROUTE_PATH).handler(routingContext -> {
                handler.handle(routingContext.request());
            });

            BlastProperties properties = blastServer.getProperties();
            HttpServerOptions options = new HttpServerOptions();
            options.setAcceptBacklog(properties.getEndpoint().getBacklog());
            options.setPort(properties.getEndpoint().getPort());
            // Don't time out, need to do something with ping here
            options.setIdleTimeout(0);
            options.setReuseAddress(properties.isReuseAddress()).setReceiveBufferSize(properties.getInputBufferSize());
            options.setSendBufferSize(properties.getOutputBufferSize()).setSoLinger(properties.getSoLinger());
            options.setTcpNoDelay(properties.isNoDelay());

            blastServer.startup();
            vertx.createHttpServer(options).requestHandler(router::accept).listen();

        } catch (BlastException ex) {
            Blast.logger.warn("Can't start Blast!", ex);
        }
    }
}
